#pragma once
#include "SFML/Graphics/Drawable.hpp"
class GameWorld;
using namespace sf;
class IGameObject :public Drawable
{
public:
	virtual void update(float DeltaTime) = 0;
	
	virtual void draw(RenderTarget& target, RenderStates states) const = 0;

	GameWorld*World;
protected:
	
};