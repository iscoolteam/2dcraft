#pragma once
#include "SFML/Graphics/Drawable.hpp"
#include "SFML/Graphics/VertexArray.hpp"
using namespace sf;

const int ChunkSize = 16;
const float TileSize = 20;
enum class ETileType : char
{
	Air, Stone, Dirt, Grass, Lava, null
};

struct Tile
{
	ETileType Type;
};

struct AABB;
class Chunk : public Drawable
{
public:
	//Drawable interface
	virtual void draw(RenderTarget& target, RenderStates states) const;

	//use random generation to create the tiles
	virtual void generateTiles();
	// reload the vertex array for drawing
	virtual void regenerateVertexArray();
	
	
	//try to load from a file
	virtual bool load(int x, int y, bool bForceRebuild);

	//save the chunk to a file
	virtual void save();

	//clear the chunk
	virtual void clear();

	//return the bounds for the tile at that location, if the point is air, return the box with 0 size
	virtual AABB getTileBounds(const Vector2f location) const;	

	virtual void setTileType(int x, int y, ETileType type);
	virtual ETileType getTileType(int x, int y) const;


	//sets the location of the chunk, it also sets the world location
	virtual void setChunkLoc(int x, int y);

	//returns location of the chunk related to other chunks(array idx)
	virtual Vector2i getChunkLoc(){ return ChunkLoc; }

	//returns global location of the chunk
	inline virtual Vector2f getLocation() const{ return Location; }

	//for physics
	inline float Top() const   { return Location.y; }
	inline float Bottom() const{ return Location.y + ChunkSize *TileSize; }
	inline float Left() const  { return Location.x; }
	inline float Right() const { return Location.x + ChunkSize *TileSize; }
private:

	//Texture to use for the tiles
	Texture *TileMap;

	//coordinate in the chunk array(for save and load)
	Vector2i ChunkLoc;

	//coordinate in world space for rendering
	Vector2f Location;
	
	//tile array
	Tile Tiles[ChunkSize][ChunkSize];

	VertexArray Vertices;

};

