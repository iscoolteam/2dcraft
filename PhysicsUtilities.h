
#pragma once

#include "SFML/System/Vector2.hpp"

using namespace sf;
struct AABB
{
	Vector2f Center;
	Vector2f Size;

	AABB(Vector2f center, Vector2f size) : Center(center), Size(size){}
	AABB() : Center(Vector2f(0, 0)), Size(Vector2f(0, 0))    {}

	//simpler access
	float Top()  { return Center.y - (Size.y / 2);}
	float Down() { return Center.y + (Size.y / 2);}
	float Left() { return Center.x - (Size.x / 2);}
	float Right(){ return Center.x + (Size.x / 2);}
};

struct SimpleCircle
{
	Vector2f Center;
	float Radius;

	SimpleCircle(Vector2f center, float radius) : Center(center), Radius(radius){};
	SimpleCircle() : Center(Vector2f(0, 0)), Radius(0){};
};

struct HitResult
{
	Vector2f HitLocation;
	Vector2f HitNormal;
};

class GameWorld;
//class made of helper static methods for physics detection, all static so they can accesed from anywhere, C style, a few will be reusable for later projects
class PhysicsUtilities
{
public:
	

	// check a line beetween 2 points, return hit point or nothing
	static HitResult simpleRaycast(const GameWorld* World ,Vector2f start, Vector2f end);

	// check if that point is inside a solid surface or not, return true if the point is inside a solid tile
	static bool isPointBlocked(const GameWorld* World,Vector2f point);

	//check if a box is blocked by a solid surface, the box is defined by the center and the size
	static bool isBoxBlocked(const GameWorld* World,Vector2f center, Vector2f size);
	static bool isBoxBlocked(const GameWorld* World,AABB box);

	//check a circle moving along a line, capsule raycast, returns the hit point
	static Vector2f circleSweep(const GameWorld* World,Vector2f start, Vector2f end, float radius);
	static Vector2f circleSweep(const GameWorld* World, SimpleCircle circle, Vector2f end) ;

	//check a box moving along a line, returns the hit point
	static HitResult boxSweep(const GameWorld* World, Vector2f start, Vector2f end, Vector2f size);
	static HitResult boxSweep(const GameWorld* World, Vector2f start, Vector2f end, AABB box);


	//check a box to other box, return true if the boxes overlap
	static bool boxIntersection(Vector2f centerA, Vector2f sizeA, Vector2f centerB, Vector2f sizeB);
	static bool boxIntersection(AABB boxA, AABB boxB);

	//check a circle to other circle, return true if they overlap
	static bool circleIntersection(Vector2f centerA, Vector2f centerB, float radiusA, float radiusB);
	static bool circleIntersection(SimpleCircle circleA, SimpleCircle circleB);

	
	//Helpers
	static bool pointInsideBox(Vector2f point, AABB box);
	static bool pointInsideBox(Vector2f point, Vector2f center, Vector2f size);
	static int boxesRelativePosition(Vector2f centerA, Vector2f sizeA, Vector2f centerB, Vector2f sizeB);
	static int boxesRelativePosition(AABB boxA, AABB boxB);
	static int lineIntersectionCircle(Vector2f circleCenter, float circleRadius, Vector2f start, Vector2f end, Vector2f& intersectionA, Vector2f& intersectionB);
	static float distancepoints(Vector2f pointA, Vector2f pointB);
	static HitResult lineIntersectionBox(Vector2f start, Vector2f end, AABB box);
	static AABB getTileBounds(const GameWorld* World,Vector2f location);

	static float VectorDistance(Vector2f Vec1, Vector2f Vec2);
};

