#pragma  once
#include "GameObject.h"
#include "SFML/Graphics.hpp"
#include "GameWorld.h"
#include "PhysicsComponent.h"
#include <memory>
using namespace sf;
class Creature : public IGameObject
{
public:

	//IGameObject interface------------------
	virtual void update(float DeltaTime);
	virtual void draw(RenderTarget& target, RenderStates states) const override;

	//base constructor and destructor
	Creature(Vector2f startLocation, GameWorld *theWorld);	
	Creature(){};
	~Creature();

	//subclass this to add new stuff when the object is created
	virtual void init(Vector2f startLocation, GameWorld*theWorld);

	virtual void setVelocity(Vector2f newVel);
	virtual Vector2f getVelocity();
	virtual void setPosition(Vector2f newPos);
	virtual Vector2f getPosition();

	virtual void createPhysicsComponent(AABB PhysBox);

	virtual void jump(float force);
	virtual void loadCharacter(std::string charname);
protected:
	std::unique_ptr<PhysicsComponent> PhysComponent;	
	
	CircleShape myCircle;
};