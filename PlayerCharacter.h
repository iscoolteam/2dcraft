#pragma once

#include "Creature.h"
#include "SFML/Graphics/Sprite.hpp"
#include <string>
#include <map>


class PlayerCharacter : public Creature
{
public:
	virtual void update(float DeltaTime);
	virtual void draw(RenderTarget& target, RenderStates states) const override;

	PlayerCharacter(Vector2f startLocation, GameWorld *theWorld);
	PlayerCharacter(){};
	
	virtual void init(Vector2f startLocation, GameWorld*theWorld) override;
	virtual void loadCharacter(std::string charname) override;
protected:
	GameWorld* theWorld;
	sf::Texture * tex;
	sf::Sprite playerSprite;
	void setSScale();
	void setSFrame();
	
	struct {
		int x;
		int y;
		int frame;
		int maxFrames;
		int frameWidth;
		std::string texturePath;
	} spriteData;
	sf::Clock frameClock;
};