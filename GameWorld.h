
#pragma once
#include "SFML/Graphics.hpp"
#include "Chunk.h"
#include "GameObject.h"

using namespace sf;

class GameWorld :public IGameObject
{
public:
	//IGameObject interface
	virtual void draw(RenderTarget& target, RenderStates states) const override;
	virtual void update(float DeltaTime) override;

	//check the bounds of the world and move chunks if needed, takes the view
	virtual void checkChunkBounds(Vector2f topleft, Vector2f botright);

	//create a chunk map of sizex, sizey dimensions, it will try to load the chunks from file
	virtual void createChunks(int sizex, int sizey);
	//save all the chunks in files
	virtual bool saveChunks();

	virtual Chunk* getChunk(Vector2f position) const ;
	virtual Chunk* getChunkFromCoords(int x, int y) const;

	
	//returns a std vector with all the AABBs of the tiles inside the rectangle defined by the function arguments. Air tiles wont return AABB
	virtual std::vector<AABB> getTilesInsideRectangle(float Top, float Bottom, float Left, float Right) const;

	//add a game object to the world
	virtual void RegisterGameObject(IGameObject * newObject);
	//remove a game object from the world
	virtual void RemoveGameObject(IGameObject * object);
	void loadCharsFile();
	std::string getTexturePath(std::string charName);
private:

	//chunk position variables for easier checks when moving the view and resetting chunks
	int leftChunk;
	int rightChunk;
	int topChunk;
	int botChunk;

	//averaged center of the world, for the chunk recentering
	Vector2f TerrainCenter;

	//chunk list
	std::vector<Chunk*> Chunks;
	//gameobject list
	std::vector<IGameObject*> GameObjects;

	std::map<std::string, std::string> charTexturePaths;
	
};

