#include "PlayerController.h"
#include "DebugDraw.h"
#include "PhysicsUtilities.h"
#include "PlayerCharacter.h"

#include <math.h>
void PlayerController::ProcessInput(float DeltaTime)
{
	sf::Vector2i Mousepos = sf::Mouse::getPosition(*myWindow);
	Vector2f mouseloc(Mousepos.x, Mousepos.y);
	mouseloc += myView->getCenter() - myView->getSize() / 2.f;


	if (sf::Mouse::isButtonPressed(Mouse::Left))
	{
		ChangeTile(mouseloc, ETileType::Air);
	}	
	else if (sf::Mouse::isButtonPressed(Mouse::Right))
	{
		ChangeTile(mouseloc, ETileType::Stone);
	}
	//creature input
	if (myCreature != nullptr)
	{

		Vector2f newVel = myCreature->getVelocity();
		if (sf::Keyboard::isKeyPressed(Keyboard::W))
		{
			myCreature->jump(200);
			newVel = myCreature->getVelocity();
		}		
		if (sf::Keyboard::isKeyPressed(Keyboard::A))
		{
			if (sf::Keyboard::isKeyPressed(Keyboard::LShift))
			{
				newVel.x = -200;
			}
			else
			newVel.x = -100;

		}
		if (sf::Keyboard::isKeyPressed(Keyboard::D))
		{
			if (sf::Keyboard::isKeyPressed(Keyboard::LShift))
			{
				newVel.x = 200;
			}
			else
				newVel.x = 100;
		}
		myCreature->setVelocity(newVel);
	
		myView->setCenter(myCreature->getPosition());
	}
}

void PlayerController::ChangeTile(Vector2f Coords, ETileType type)
{
	Chunk * chosenchunk = World->getChunk(Coords);

	if (chosenchunk)
	{
		float realx = Coords.x - chosenchunk->getLocation().x;
		float realy = Coords.y - chosenchunk->getLocation().y;

		int finalx = int(realx / TileSize);
		int finaly = int(realy / TileSize);
		chosenchunk->setTileType(finalx, finaly, type);
	}
}

void PlayerController::ProcessInputEvent(Event event)
{
	
	if (event.type == sf::Event::Closed)
	{
		myWindow->close();
	}
	// catch the resize events
	if (event.type == sf::Event::Resized)
	{
		// update the view to the new size of the window
		sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);
		myView->reset(visibleArea);
		
	}
	if (event.type == sf::Event::KeyPressed)
	{
		if (event.key.code == sf::Keyboard::Escape)
		{
			myWindow->close();
		}
		else if (event.key.code == sf::Keyboard::Left)
		{
			myView->move(30.f, 0.f);
		}
		else if (event.key.code == sf::Keyboard::Right)
		{
			myView->move(-30.f, 0.f);
		}
		else if (event.key.code == sf::Keyboard::Space)
		{
			World->saveChunks();
		}
		
	}
	sf::Vector2i Mousepos = sf::Mouse::getPosition(*myWindow);
	Vector2f mouseloc(Mousepos.x, Mousepos.y);
	mouseloc += myView->getCenter() - myView->getSize() / 2.f;
	if (event.type == Event::MouseButtonPressed && event.mouseButton.button == Mouse::Button::Right)
	{	

		if (!start)
		{
			lineend = mouseloc;
			start = !start;
		}
		else
		{
			start = !start;
			linestart = mouseloc;
		}
	}

	if ((event.type == Event::MouseButtonPressed && event.mouseButton.button == Mouse::Button::Middle) || (event.type == Event::KeyPressed) &&(event.key.code == sf::Keyboard::F3))
	{
		if (!PhysicsUtilities::isPointBlocked(World,mouseloc))
		{
			if (myCreature != nullptr)
			{
				delete myCreature;
			}

			myCreature = new PlayerCharacter(mouseloc, World);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) {
				myCreature->loadCharacter("mario");
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) {
				myCreature->loadCharacter("luigi");
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3)) {
				myCreature->loadCharacter("neardental");
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num4)) {
				myCreature->loadCharacter("pikachu");
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num5)) {
				myCreature->loadCharacter("sonic");
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num6)) {
				myCreature->loadCharacter("smurf");
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num7)) {
				myCreature->loadCharacter("spiderman");
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num8)) {
				myCreature->loadCharacter("venom");
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num9)) {
				myCreature->loadCharacter("goomba");
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num0)) {
				myCreature->loadCharacter("batman");
			}
		}
	}
	if (event.type == Event::KeyPressed && event.key.code == sf::Keyboard::F4) {
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) {
			myCreature->loadCharacter("mario");
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) {
			myCreature->loadCharacter("luigi");
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3)) {
			myCreature->loadCharacter("neardental");
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num4)) {
			myCreature->loadCharacter("pikachu");
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num5)) {
			myCreature->loadCharacter("sonic");
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num6)) {
			myCreature->loadCharacter("smurf");
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num7)) {
			myCreature->loadCharacter("spiderman");
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num8)) {
			myCreature->loadCharacter("venom");
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num9)) {
			myCreature->loadCharacter("goomba");
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num0)) {
			myCreature->loadCharacter("batman");
		}
		//else
		
	}

}

PlayerController::PlayerController(GameWorld* world, Window*window, View* view)
{
	myCreature = nullptr;
	start = false;
	World = world;
	myWindow = window;
	myView = view;
}
