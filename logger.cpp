﻿#include "logger.h"


Logger::Logger() {
	prepareEnviroment();
}

Logger * Logger::Instance = nullptr;
Logger * Logger::Get()
{
	if (Instance == nullptr)
	{
		Instance = new Logger();
	}

	return Instance;
}




void Logger::prepareEnviroment() {
	//system("color 0F"); //Set window colors
	setOutColor(tColor::light_green, tColor::black); //Set output color
	executeCommand("title 2DCraft: logger"); //Set window title
	ShowBlinkingCursor(false);
}

void Logger::setOutColor(tColor font, tColor background) {
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(handle, (int(font) + int(background) * 16));
}


void Logger::executeCommand(std::string cmd) {
	system(cmd.c_str());
}

void Logger::consoleGotoXY(int x, int y) {
	//Initialize the coordinates
	COORD coord = { x, y };
	//Set the position
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

void Logger::ShowBlinkingCursor(bool showFlag) {
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_CURSOR_INFO     cursorInfo;

	GetConsoleCursorInfo(out, &cursorInfo);
	cursorInfo.bVisible = showFlag; // set the cursor visibility
	SetConsoleCursorInfo(out, &cursorInfo);
}


std::string Logger::getTime() {
	time_t date;
	date = time(0);
	std::stringstream result;
	tm ltm;
	localtime_s(&ltm, &date);
	result << ltm.tm_hour << ":" << ltm.tm_min << ":" << ltm.tm_sec;
	return result.str();
}

std::string Logger::getStrFromLogLevel(tLogLevel ll) {
	std::string str;
	switch (ll)
	{
	case tLogLevel::LOG_DEBUG:
		str = "DEBUG";
		break;
	case tLogLevel::LOG_INFO:
		str = "INFO";
		break;
	case tLogLevel::LOG_WARNING:
		str = "WARN";
		break;
	case tLogLevel::LOG_ERROR:
		str = "ERROR";
		break;
	default:
		str = "UNKNW";
		break;
	}
	return str;
}

tColor Logger::getColorFromLogLevel(tLogLevel ll) {
	tColor color;
	switch (ll)
	{
	case tLogLevel::LOG_DEBUG:
		color = tColor::light_green;
		break;
	case tLogLevel::LOG_INFO:
		color = tColor::light_blue;
		break;
	case tLogLevel::LOG_WARNING:
		color = tColor::light_yellow;
		break;
	case tLogLevel::LOG_ERROR:
		color = tColor::light_red;
		break;
	default:
		color = tColor::light_magenta;
		break;
	}
	return color;
}


void Logger::log(tLogLevel logLevel, std::string str) {
	
	oFile.open("log.txt", std::fstream::app);

	std::string fileLog = "";
	std::string time = getTime();
	setOutColor(tColor::white, tColor::black);

	std::cout << "[";
	setOutColor(getColorFromLogLevel(logLevel), tColor::black);
	std::cout << std::setw(5) << std::left << getStrFromLogLevel(logLevel);
	setOutColor(tColor::white, tColor::black);
	std::cout << "]";
	oFile << "[" << std::setw(5) << std::left << getStrFromLogLevel(logLevel) << "]";

	std::cout << "[" << std::setw(8) << std::left << time << "] ";
	oFile << "[" << std::setw(8) << std::left << time << "] ";

	std::cout << str << std::endl;
	oFile << str << std::endl;

	oFile.close();
}
