#include "PlayerCharacter.h"
#include "ResourceManager.h"
#include <fstream>
#include "logger.h"
void PlayerCharacter::draw(RenderTarget& target, RenderStates states) const
{
	
	target.draw(playerSprite, states);
}

void PlayerCharacter::update(float DeltaTime)
{
	Creature::update(DeltaTime);
	playerSprite.setPosition(PhysComponent->getPosition());
	setSScale();
	setSFrame();
}

PlayerCharacter::PlayerCharacter(Vector2f startLocation, GameWorld *theWorld)
{
	init(startLocation, theWorld);	
}


void PlayerCharacter::setSScale() {
	//playerSprite.setTextureRect(sf::IntRect(h * TileSize * 2, v * TileSize * 2, TileSize * 2, TileSize * 2));
	if (getVelocity().x >= 0) spriteData.x = 1;
	else spriteData.x = -1;
	if (getVelocity().y > -0.5) spriteData.y = 1;
	//else spriteData["y"] = 0.7;

	playerSprite.setScale(sf::Vector2f(spriteData.x, spriteData.y));
}


void PlayerCharacter::setSFrame() {
	if (abs(getVelocity().x) > 0.1) {
		if (frameClock.getElapsedTime().asMilliseconds() > 100) {
			spriteData.frame++;
			frameClock.restart();
			if (spriteData.frame >= spriteData.maxFrames) spriteData.frame = 0;
		}
	} else spriteData.frame = 2;
	playerSprite.setTextureRect(sf::IntRect(spriteData.frame * TileSize * spriteData.frameWidth, 0, TileSize * spriteData.frameWidth, TileSize * 2));
}

void PlayerCharacter::init(Vector2f startLocation, GameWorld*theWorld)
{
	Creature::init(startLocation, theWorld);
	
	this->theWorld = theWorld;

	//spriteData.texturePath = "Resources/characters/playerMario.png";
	//sf::Texture * tex = ResourceManager::Get()->GetTexture(spriteData.texturePath);
	loadCharacter("mario");
	spriteData.x = 1;
	spriteData.y = 1;
	spriteData.frame = 0;
	spriteData.maxFrames = 7;
	spriteData.frameWidth = 1;

	//playerSprite.setTexture(*tex, true);
	playerSprite.setOrigin(sf::Vector2f(10, 20));
	playerSprite.setOrigin(Vector2f(TileSize * (spriteData.frameWidth / 2.0), TileSize));

	spriteData.frameWidth = 1;	
	AABB bounds(startLocation, sf::Vector2f(18, 35));
	PhysComponent->setBounds(bounds);
}

void PlayerCharacter::loadCharacter(std::string charname) {
	Logger::Get()->log(tLogLevel::LOG_DEBUG, "Changing character to: " + charname);
	spriteData.texturePath = theWorld->getTexturePath(charname);
	tex = ResourceManager::Get()->GetTexture(spriteData.texturePath);
	playerSprite.setTexture(*tex, true);
}

