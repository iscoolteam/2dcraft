#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <Windows.h>

enum class tColor {
	black,
	dark_blue,
	dark_green,
	dark_cyan,
	dark_red,
	dark_magenta,
	dark_yellow,
	light_grey,
	dark_gray,
	light_blue,
	light_green,
	light_cyan,
	light_red,
	light_magenta,
	light_yellow,
	white
};
enum class tLogLevel{ LOG_DEBUG, LOG_INFO, LOG_WARNING, LOG_ERROR };

class Logger {
private:
	Logger();
	
	std::ofstream oFile;

	void prepareEnviroment();
	void setOutColor(tColor font, tColor background);
	void setBuffer(int lines);
	void executeCommand(std::string cmd);
	void consoleGotoXY(int x, int y);
	void ClearConsole();
	void ShowBlinkingCursor(bool showFlag);
	std::string getTime();
	std::string getStrFromLogLevel(tLogLevel ll);
	tColor getColorFromLogLevel(tLogLevel ll);

	//Singleton
	static Logger * Instance;
public:
	static Logger * Get();
	void log(tLogLevel logLevel, std::string str);
};