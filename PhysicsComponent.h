#pragma once
#include "PhysicsUtilities.h"
#include "GameObject.h"

class PhysicsComponent
{
public:

	//constructors
	PhysicsComponent(AABB bBox, IGameObject *newParent);
	PhysicsComponent(Vector2f Center, Vector2f Size, IGameObject *newParent);


	void setVelocity(Vector2f newVelocity);
	Vector2f getVelocity();

	//teleport the physics comp to that position, if check collisions, dont teleport if the place is blocked
	void setPosition(Vector2f newPosition,bool checkCollisions = false);
	Vector2f getPosition();

	void setGravity(float newGravity);
	float getGravity();

	AABB getBounds();
	void setBounds(AABB newBounds);

	//checks a bit below the bounds, and if its ground that check it will return true
	bool isOnTheGround();

    void setParent(IGameObject*newParent);
	//add the offset to the position, if check collisions, raytrace that offset and move until its blocked
	void move(Vector2f offset, bool checkCollisions = false);

	//add gravity and move the physicscomp with the current velocity
	void update(float DeltaTime);
    
protected:
	Vector2f velocity;
	AABB boundingBox;
	float gravity{ 98 };
	IGameObject *parent{nullptr};
};