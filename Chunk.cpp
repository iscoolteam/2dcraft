#include "Chunk.h"
#include "SFML/Graphics/RenderTarget.hpp"
#include <iostream>
#include "PhysicsUtilities.h"
#include <fstream>
#include <string>
#include <sstream>
#include "ResourceManager.h"
#include "noise/noise.h"
#include <cmath>
using namespace std;
void Chunk::draw(RenderTarget& target, RenderStates states) const
{
	states.texture = TileMap;
	target.draw(Vertices, states);
}

void Chunk::generateTiles()
{
	//uwotm8
	noise::module::Perlin PerlinNoise = noise::module::Perlin();

	//PerlinNoise.SetSeed(0);
	PerlinNoise.SetOctaveCount(2);
	//PerlinNoise.SetFrequency(10);
	double value = PerlinNoise.GetValue(1.25, 0.75, 0.50);
	
	for (int x = 0; x< ChunkSize; x++)
	{
		bool TopLayer = false;
		for (int y = 0; y < ChunkSize; y++)
		{
			float Xnoise = x + Location.x/TileSize;
			float Ynoise = y + Location.y/TileSize;

			//this shit creates mountains
			float Density = PerlinNoise.GetValue(Xnoise/10, Ynoise/10, 0);			
			Density += (Ynoise/5)-1.5;
		
			if (Density > 15)
			{

			}
			else if (Density > 1)
			{
				Density = 1;

				//add holes as caves
				float val = PerlinNoise.GetValue(Xnoise / 10, Ynoise / 10, 3.4);				
				if (val > 0.5)
				{
				Density = 0;
				}
				else
				{
					Density -= val;
				}			
			}
			
			

			//std::cout << Density << "-" << Xnoise << "-" << Ynoise << std::endl;
			if (Density > 15)
			{
				Tiles[x][y].Type = ETileType::Lava;
			}
			else if (Density> 0.6)
			{
				Tiles[x][y].Type = ETileType::Stone;
			}
			else if (Density > 0.3)
			{
				Tiles[x][y].Type = ETileType::Dirt;
			}
			else if (Density > 0)
			{
				Tiles[x][y].Type = ETileType::Grass;
			}
			else
			{
				Tiles[x][y].Type = ETileType::Air;
			}
			//Tiles[x][y].Type = ETileType::Type((x*y) % 3);
		}
	}

	
	regenerateVertexArray();
}

void Chunk::regenerateVertexArray()
{

	TileMap = ResourceManager::Get()->GetTexture( "Resources/Tiles.png");

	Vertices.resize(ChunkSize*ChunkSize * 4);
	Vertices.setPrimitiveType(PrimitiveType::Quads);
	
	for (int x = 0; x < ChunkSize; x++)
	{
		for (int y = 0; y < ChunkSize; y++)
		{
			Vector2f A, B, C, D;

			A.x = x*TileSize-1;
			A.y = y*TileSize-1;
			A += Location;

			B.x = (x+1)*TileSize+1;
			B.y = y*TileSize-1;
			B += Location;

			C.x = x*TileSize-1;
			C.y = (y + 1)*TileSize+1;
			C += Location;

			D.x = (x + 1)*TileSize+1;
			D.y = (y+1)*TileSize+1;
			D += Location;

			Color VertColor = Color::White;

			//texture coords
			Vector2f tA, tB, tC, tD;

			switch (Tiles[x][y].Type)
			{
			case ETileType::Air:
				VertColor = Color::Transparent;
				tA = Vector2f(0, 32);
				tB = Vector2f(32, 32);
				tC = Vector2f(32, 64);
				tD = Vector2f(0, 64);

				break;
			case ETileType::Dirt:
				//VertColor.r = 133;
				//VertColor.g = 65;
				//VertColor.b = 35;

				tA = Vector2f(0, 0);
				tB = Vector2f(32, 0);
				tC = Vector2f(32, 32);
				tD = Vector2f(0, 32);
				break;
			case ETileType::Stone:
				//VertColor.r = 100;
				//VertColor.g = 100;
				//VertColor.b = 100;

				tA = Vector2f(32, 0);
				tB = Vector2f(64, 0);
				tC = Vector2f(64, 32);
				tD = Vector2f(32, 32);
				break;
			case ETileType::Lava:
				VertColor.r = 255;
			    VertColor.g = 100;
				VertColor.b = 100;

				tA = Vector2f(32, 0);
				tB = Vector2f(64, 0);
				tC = Vector2f(64, 32);
				tD = Vector2f(32, 32);
				break;

			case ETileType::Grass:
				tA = Vector2f(32, 32);
				tB = Vector2f(64, 32);
				tC = Vector2f(64, 64);
				tD = Vector2f(32, 64);
				break;
			default:
				VertColor = Color::Red;
			}
		//	VertColor = Color::White;
			sf::Vertex* quad = &Vertices[(y + x * ChunkSize) * 4];

			quad[0] = Vertex(A, VertColor, tA);
			quad[1] = Vertex(B, VertColor,tB);
			quad[2] = Vertex(D, VertColor, tC);
			quad[3] = Vertex(C, VertColor,tD);
		}
	}	
}

void Chunk::setTileType(int x, int y, ETileType type)
{
	if (x < ChunkSize && y < ChunkSize && x >= 0 && y >= 0)
	{
		if (Tiles[x][y].Type != type)
		{
			Tiles[x][y].Type = type;
			regenerateVertexArray();
		}
	}
	else
	{
		std::cout << " invalid coordinates "<< std::endl;
	}
}

bool Chunk::load(int x, int y, bool bForceRebuild)
{
	setChunkLoc(x, y);

	if (!bForceRebuild)
	{
		stringstream ss;

		ss << "Saves/chunk" << x << "-" << y << ".cnkb";
		string filename = ss.str();
		//open the file in binary mode
		ifstream file(filename,ios::in | ios::binary);
		
		if (file.is_open())
		{			
			//this brutality copies the file data into the tiles array
			file.read((char*)Tiles, sizeof(char)*ChunkSize*ChunkSize);
			regenerateVertexArray();
		}
		else
		{
			generateTiles();
		}
	}
	else
	{
		generateTiles();
	}	
	return true;
}

void Chunk::save()
{	
	stringstream ss2;
	ss2 << "Saves/chunk" << ChunkLoc.x << "-" << ChunkLoc.y << ".cnkb";
	string fileame = ss2.str();

	//open the file in binary mode
	ofstream myFile(fileame, ios::out | ios::binary);
	if (myFile.is_open())
	{	
		//copypaste the Tiles array into a file
		myFile.write( (char*)Tiles, sizeof(char)*ChunkSize*ChunkSize);
	}
	myFile.close();
}

void Chunk::clear()
{
	for (int x = 0; x < ChunkSize; x++)
	{
		for (int y = 0; y < ChunkSize; y++)
		{
			Tiles[x][y].Type = ETileType::Air;
		}
	}
	setChunkLoc(0, 0);
}

void Chunk::setChunkLoc(int x, int y)
{
	ChunkLoc.x = x; ChunkLoc.y = y;

	Location.x = x*ChunkSize*TileSize;
	Location.y = y*ChunkSize*TileSize;

	regenerateVertexArray();
}

ETileType Chunk::getTileType(int x, int y) const
{
	if (x < ChunkSize && y < ChunkSize && x >= 0 && y >= 0)
	{
		return Tiles[x][y].Type;
	}

	return ETileType::null;
}

AABB Chunk::getTileBounds(const Vector2f location) const
{
	AABB result;
	
	//Get the real array coordinates from the world location
	const float realx = location.x - getLocation().x;
	const float realy = location.y - getLocation().y;

	const int finalx = (realx / TileSize);
	const int finaly = (realy / TileSize);

	ETileType type = getTileType(finalx, finaly);

	if (type == ETileType::Air || type == ETileType::null)
	{
		//air or nothing, so there is no collision bounds
		return result;
	}
	else
	{
		//give the collision box its proper values
		Vector2f center;
		center.x = finalx * TileSize + TileSize / 2;
		center.y = finaly * TileSize + TileSize / 2;

		result.Center = center + getLocation();
		result.Size = Vector2f(TileSize, TileSize);
	}

	return result;
}
