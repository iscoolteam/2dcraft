#include "GameWorld.h"
#include "DebugDraw.h"
#include "logger.h"
#include <iostream>
#include "PhysicsUtilities.h"
void GameWorld::draw(RenderTarget& target, RenderStates states) const
{
	//draw the tiles
	for (auto chunk : Chunks)
	{
		target.draw(*chunk);
	}
	//draw game objects
	for (auto object : GameObjects)
	{
		object->draw(target, states);
	}

	//draw the debugdraw stuffs
	target.draw(*DebugDraw::Get());
	//clear debugdraw data
	DebugDraw::Get()->clearArray();
}

void GameWorld::update(float DeltaTime)
{
	for (auto object : GameObjects)
	{
		object->update(DeltaTime);
	}
}

void GameWorld::createChunks(int sizex, int sizey)
{
	leftChunk = 0;
	topChunk = 0;
	botChunk = sizey-1;
	rightChunk = sizex - 1;

	//reserve some space for the chunks
	Chunks.reserve(sizex * sizey);
	for (int i = 0; i < sizex; i++)
	{
		for (int j = 0; j < sizey; j++)
		{
			Chunk *newChunk = new Chunk();			
			newChunk->load(i, j, false);
			
			Chunks.push_back(newChunk);
		}
	}

	TerrainCenter.x =( sizex*ChunkSize*TileSize)/2 ;
	TerrainCenter.y = (sizey*ChunkSize*TileSize) / 2;
}

Chunk* GameWorld::getChunk(Vector2f position)const
{
	Chunk * chosenchunk = nullptr;
	for (auto chunk : Chunks)
	{
		//y axis
		if (chunk->Top() <= position.y && chunk->Bottom() >= position.y)
		{
			// x axis
			if (chunk->Left() <= position.x && chunk->Right() >= position.x)
			{
				chosenchunk = chunk;
				return chosenchunk;
				break;
			}
		}
	}
	return chosenchunk;
}
Chunk* GameWorld::getChunkFromCoords(int x, int y)const
{
	Chunk * chosenchunk = nullptr;

	Vector2f position;
	position.x = x*ChunkSize*TileSize + 5;
	position.y = y*ChunkSize*TileSize + 5;

	chosenchunk = getChunk(position);

	return chosenchunk;
}


bool GameWorld::saveChunks()
{
	for (auto chunk : Chunks)
	{
		chunk->save();
	}
	return true;
}

void GameWorld::checkChunkBounds(Vector2f topleft, Vector2f botright)
{
	float left = leftChunk * TileSize*ChunkSize;
	float right = rightChunk + 1 * TileSize*ChunkSize;
	float bot = botChunk * TileSize*ChunkSize;
	float top = topChunk * TileSize*ChunkSize;
	Chunk * thechunk = nullptr;
	
	
	if (topleft.x < TerrainCenter.x - 4*TileSize*ChunkSize)
	{
		//move left
		bool moved = false;
		for (int i = topChunk; i <= botChunk; i++)
		{
			thechunk = getChunkFromCoords(rightChunk, i);
			if (thechunk)
			{
				thechunk->save();
				thechunk->clear();
				thechunk->load(leftChunk - 1, i, false);
				moved = true;

				Logger::Get()->log(tLogLevel::LOG_DEBUG, "Moved a chunk: LEFT");
			}
		}
		if (moved)
		{
			leftChunk--;
			rightChunk--;
			TerrainCenter.x -= TileSize*ChunkSize;
		}

	}
	else if (botright.x > TerrainCenter.x + 4*TileSize*ChunkSize)
	{
		//move left
		bool moved = false;
		for (int i = topChunk; i <= botChunk; i++)
		{
			Vector2f position;
			position.x = leftChunk*ChunkSize*TileSize + 5;
			position.y = i*ChunkSize*TileSize + 5;

			thechunk = getChunk(position);


			//thechunk = getChunkFromCoords(leftChunk, i);
			if (thechunk)
			{
				thechunk->save();
				thechunk->clear();
				thechunk->load(rightChunk+  1, i, true);
				moved = true;
				Logger::Get()->log(tLogLevel::LOG_DEBUG, "Moved a chunk: RIGHT");
			}
		}
		if (moved)
		{
			leftChunk++;
			rightChunk++;
			TerrainCenter.x += TileSize*ChunkSize;
		}
	}	
}

std::vector<AABB> GameWorld::getTilesInsideRectangle(float Top, float Bottom, float Left, float Right) const
{
	std::vector<AABB> Tiles;


	//reserve the maximum size the tiles could be, so we dont do too many resizes with the push_back function
	Tiles.reserve(((Bottom - Top) / TileSize + 1)  *((Right - Left) / TileSize + 1));

	//get chunks that are in those bounds, then get the tiles of those
	for (auto chunk : Chunks)	
	{				
	 	if (!( Bottom < chunk->Top()  //intersection beetween the chunk bounds and the selection rectangle
			|| Top    > chunk->Bottom()
			|| Left   > chunk->Right()
			|| Right  < chunk->Left())
			  )
		{
			//get the tiles inside that rectangle
			for (float y = Top; y < Bottom; y += TileSize)
			{
				for (float x = Left; x < Right; x += TileSize)
				{
					AABB box = chunk->getTileBounds(sf::Vector2f(x, y));
					if (box.Center != Vector2f(0, 0))
					{
						Tiles.push_back(box);						
					}
				}
			}			
		}					
	}	
	return Tiles;
}

void GameWorld::RegisterGameObject(IGameObject * newObject)
{
	for (int i = 0; i < GameObjects.size(); i++)
	{
		if (GameObjects[i] == newObject)
		{
			Logger::Get()->log(tLogLevel::LOG_ERROR, "WTF this object is already in the array");
			return;
		}
	}
	GameObjects.push_back(newObject);
}

void GameWorld::RemoveGameObject(IGameObject * object)
{
	for (int i = 0; i < GameObjects.size(); i++)
	{
		if (GameObjects[i] == object)
		{
			//swap for the last member
			GameObjects[i] = GameObjects[GameObjects.size() - 1];
			GameObjects.pop_back();
			return;
		}
	}
}

void GameWorld::loadCharsFile(){
	std::ifstream iFile("Resources/characters.txt");
	std::string charName, tmp;
	if (iFile.is_open()) {
		iFile >> charName;
		while (charName != "-") {
			if (charName[0] != '#') {
				iFile.get();
				getline(iFile, tmp);
				Logger::Get()->log(tLogLevel::LOG_DEBUG, " Reading characters file: " + charName + "-> " + tmp);
				charTexturePaths[charName] = tmp;
			}
			else {
				getline(iFile, tmp);
			}
			iFile >> charName;
		}
		iFile.close();
	}
	else {
		Logger::Get()->log(tLogLevel::LOG_ERROR, " Couldn't open file!!! : Resources/characters.txt");
	}
}

std::string GameWorld::getTexturePath(std::string charName) {
	if (charTexturePaths.find("texturesFolder") == charTexturePaths.end()) {
		loadCharsFile();
	}
	if (charTexturePaths.find(charName) == charTexturePaths.end()) {
		return charTexturePaths["texturesFolder"] + charTexturePaths["mario"];
	}
	else {
		return charTexturePaths["texturesFolder"] + charTexturePaths[charName];
	}
}