#include "Creature.h"
#include "PhysicsUtilities.h"

#include "DebugDraw.h"
using namespace std;
void Creature::update(float DeltaTime)
{
	if (PhysComponent)
	{
		PhysComponent->update(DeltaTime);
	}
	myCircle.setPosition(getPosition());
}

void Creature::draw(RenderTarget& target, RenderStates states) const
{
	target.draw(myCircle);
}

Creature::Creature(Vector2f startLocation, GameWorld *theWorld)
{
	init(startLocation, theWorld);	
}
void Creature::init(Vector2f startLocation, GameWorld *theWorld)
{
	myCircle = CircleShape(10);
	myCircle.setOrigin(Vector2f(10, 10));
	myCircle.setPosition(startLocation);
	AABB bbox(startLocation, Vector2f(20, 20));

	createPhysicsComponent(bbox);
	setVelocity(Vector2f(0, 0));
	setPosition(startLocation);
	World = theWorld;
	World->RegisterGameObject(this);
}

void Creature::createPhysicsComponent(AABB PhysBox)
{
	PhysComponent = std::unique_ptr<PhysicsComponent>(new PhysicsComponent(PhysBox, this));
}

Creature::~Creature()
{
	World->RemoveGameObject(this);
}

void Creature::jump(float force)
{
	if (PhysComponent->isOnTheGround())
	{
		Vector2f vel = PhysComponent->getVelocity();
		vel.y = -force;
		PhysComponent->setVelocity(vel);
	}
}

sf::Vector2f Creature::getPosition()
{
	if (PhysComponent)
	{
		return PhysComponent->getPosition();
	}
	else
	{
		return Vector2f();
	}
}

sf::Vector2f Creature::getVelocity()
{
	if (PhysComponent)
	{
		return PhysComponent->getVelocity();
	}
	else
	{
		return Vector2f();
	}
}

void Creature::setVelocity(Vector2f newVel)
{
	if (PhysComponent) { PhysComponent->setVelocity(newVel); }
}

void Creature::setPosition(Vector2f newPos)
{
	if (PhysComponent) { PhysComponent->setPosition(newPos); }
}

void Creature::loadCharacter(std::string charname) {}