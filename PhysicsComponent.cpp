#include "PhysicsComponent.h"
#include "DebugDraw.h"

PhysicsComponent::PhysicsComponent(AABB bBox, IGameObject *newParent) {
   velocity = Vector2f(0, 0);
   boundingBox = bBox;
   parent = newParent;
}
PhysicsComponent::PhysicsComponent(Vector2f Center, Vector2f Size, IGameObject *newParent) {
   AABB box;
   box.Center = Center;
   box.Size = Size;
   PhysicsComponent(box, newParent);
}


void PhysicsComponent::setVelocity(Vector2f newVelocity) {
   velocity = newVelocity;
}
Vector2f PhysicsComponent::getVelocity() {
   return velocity;
}

void PhysicsComponent::setParent(IGameObject *newParent) {
   parent = newParent;
}
//teleport the physics comp to that position, if check collisions, dont teleport if the place is blocked
void PhysicsComponent::setPosition(Vector2f newPosition,bool checkCollisions) {
	if (checkCollisions) {
		if (!PhysicsUtilities::isBoxBlocked(parent->World, boundingBox.Center, boundingBox.Size)) {
			boundingBox.Center = newPosition;
		}
	}
	else
	{
		boundingBox.Center = newPosition;
	}
   
}
Vector2f PhysicsComponent::getPosition() {
   return boundingBox.Center;
}

//add the offset to the position, if check collisions, raytrace that offset and move until its blocked
void PhysicsComponent::move(Vector2f offset, bool checkCollisions) {
	Vector2f newPos = getPosition() + offset;
	Vector2f newVel = velocity;
	if (checkCollisions)
	{	
		
		HitResult Hit = PhysicsUtilities::boxSweep(parent->World, getPosition(), newPos, boundingBox);
		if (Hit.HitLocation != Vector2f(0, 0))
		{
			if (abs(Hit.HitNormal.y) > 0.1)
			{
				newVel.y = 0; 
				newPos.y = Hit.HitLocation.y;
			}
			else if (abs(Hit.HitNormal.x) > 0.1)
			{
				newVel.x = 0; 
				newPos.x = Hit.HitLocation.x;
			}
			else
			{
				newPos = Hit.HitLocation;//+ Hit.HitNormal;	
			}
		}
	}
   setVelocity(newVel);
   setPosition(newPos);
}

//add gravity and move the physicscomp with the current velocity
void PhysicsComponent::update(float DeltaTime) {
	if (!isOnTheGround())
	{
		velocity.y += gravity*DeltaTime;
	}
	else
	{
		float deceleration = 100;
		velocity -= velocity * 100.f * DeltaTime;
	}
	move(velocity*DeltaTime,true);
}

void PhysicsComponent::setBounds(AABB newBounds)
{
	boundingBox = newBounds;
}

bool PhysicsComponent::isOnTheGround()
{
	sf::Vector2f floorcheckr = getPosition();
	floorcheckr.y += boundingBox.Size.y / 2 + 1;
	floorcheckr.x += boundingBox.Size.x / 2 -2;

	sf::Vector2f floorcheckl = getPosition();
	floorcheckl.y += boundingBox.Size.y / 2 + 1;
	floorcheckl.x -= boundingBox.Size.x / 2 -2;

	//only add gravity if we are in the air
	if (PhysicsUtilities::getTileBounds(parent->World, floorcheckr).Center != sf::Vector2f(0, 0))
	{
		return true;
	}
	else if (PhysicsUtilities::getTileBounds(parent->World, floorcheckl).Center != sf::Vector2f(0, 0))
	{
		return true;
	}
	return false;
}
