#include <SFML/Graphics.hpp>
#include "Chunk.h"
#include <iostream>
#include <time.h>
#include <sstream>

#include "GameWorld.h"
#include "PlayerController.h"
#include "ResourceManager.h"
#include "logger.h"
#include "PhysicsUtilities.h"
int main()
{
	Logger::Get()->log(tLogLevel::LOG_DEBUG, " *********** START ************ ");
	bool showDebugMenu = true;
	sf::Image screenshot;
	std::stringstream timestamp;
	
	sf::RenderWindow window(sf::VideoMode(1000, 600), "2DCraft!");

	
	GameWorld World;
	World.createChunks(8, 6);

	//check physics
	AABB testbox;
	testbox.Center = Vector2f(1, 1);
	testbox.Size = Vector2f(2, 2);
	
	PhysicsUtilities::lineIntersectionBox(Vector2f(-1, 1), Vector2f(3,1), testbox);
	

	sf::View view(sf::FloatRect(0, 0, 1000, 600));
	sf::View zoomedview(sf::FloatRect( 0,-0, 1000, 600));
	
	PlayerController Controller = PlayerController(&World, &window, &view);
	//window.setFramerateLimit(60);
	//double frametime = 1.f / 60.f;

	Clock clock;
	sf::Text fpstext;

	Font *fpsfont = ResourceManager::Get()->GetFont("Resources/sansation.ttf");
	fpstext.setPosition(Vector2f(10, 5));
	fpstext.setFont(*fpsfont);
	fpstext.setColor(sf::Color::Black);

	std::vector<Creature*> creatures;
	/*for (float i = 0; i < 100; i++)
	{
		Creature * creature = new Creature(sf::Vector2f(10.f * i, 20.f), &World);
		creatures.push_back(creature);
	}
	*/

	while (window.isOpen())
	{
		double elapsedtime = clock.restart().asSeconds();


		sf::Event event;
		while (window.pollEvent(event))
		{
			
			if (event.type == sf::Event::KeyPressed){
				if (event.key.code == sf::Keyboard::F1){
					screenshot = window.capture();
					timestamp << time(0);
					if (screenshot.saveToFile("screenshots/" + timestamp.str() +".jpg")) {
						Logger::Get()->log(tLogLevel::LOG_INFO, "Screenshot saved: " + timestamp.str() + ".jpg");
					}
					else {
						Logger::Get()->log(tLogLevel::LOG_WARNING, "Screenshot wasn't saved! Create the folder 'screenshots'!");
					}
					timestamp.str(std::string());
					timestamp.clear();
				}
				if (event.key.code == sf::Keyboard::F2){
					showDebugMenu = !showDebugMenu;
				}
			}
			Controller.ProcessInputEvent(event);
		}

		Controller.ProcessInput(elapsedtime);		
		World.update(elapsedtime);
		World.checkChunkBounds(view.getCenter() - Vector2f(500, 300), view.getCenter() + Vector2f(500, 300));
		window.setView(view);
		window.clear(Color(95, 184, 213));
		
		window.draw(World);
		

		fpstext.setString(std::to_string(int(1/elapsedtime)) );
		fpstext.setPosition(view.getCenter() + Vector2f(-300,100));
	
		if (showDebugMenu) window.draw(fpstext);
		window.display();
	}
	Logger::Get()->log(tLogLevel::LOG_DEBUG, " *********** END ************ ");
	return 0;
}