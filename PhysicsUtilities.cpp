#include <cmath>
#include "PhysicsUtilities.h"
#include "GameWorld.h"
#include "DebugDraw.h"
#include "logger.h"

bool PhysicsUtilities::isPointBlocked(const GameWorld* World, Vector2f point)
{
	Chunk * chosenchunk = World->getChunk(point);

	if (chosenchunk)
	{
		DebugDraw::Get()->DrawDebugCircle(point, 5, Color::Blue);
		float realx = point.x - chosenchunk->getLocation().x;
		float realy = point.y - chosenchunk->getLocation().y;

		int finalx = (realx / TileSize);
		int finaly = (realy / TileSize);
		if (chosenchunk->getTileType(finalx, finaly) != ETileType::Air)
		{
			return true;
		}
		//Logger::Get()->log(tLogLevel::LOG_ERROR, " Box collision ");
	
	}
	return false;
}

HitResult PhysicsUtilities::simpleRaycast(const GameWorld* World, Vector2f start, Vector2f end)
{

	HitResult bestResult;
	std::vector<AABB> tiles = World->getTilesInsideRectangle(fmin(start.y, end.y) - TileSize, fmax(start.y, end.y) + TileSize, fmin(start.x, end.x) - TileSize, fmax(start.x, end.x) + TileSize);
	
	for (auto tile : tiles)
	{		
		
		HitResult result = lineIntersectionBox(start, end, tile);
		if (result.HitLocation != Vector2f(0, 0))
		{
			if (bestResult.HitLocation == Vector2f(0, 0) || VectorDistance(result.HitLocation, start) < VectorDistance(bestResult.HitLocation, start))
			{
				bestResult = result;
			}
		}				
	}


	return bestResult;
}
HitResult PhysicsUtilities::boxSweep(const GameWorld* World, Vector2f start, Vector2f end, AABB boxR)
{
	HitResult bestResult;
	AABB box = boxR; box.Size.x -= 0.5f; box.Size.y -= 0.5f;
	std::vector<AABB> tiles = World->getTilesInsideRectangle(fmin(start.y, end.y) - box.Size.y*2, fmax(start.y, end.y) + box.Size.y*2, fmin(start.x, end.x) - box.Size.x*2, fmax(start.x, end.x) + box.Size.x*2);
	
	for (auto tile : tiles)
	{
		//DebugDraw::Get()->DrawDebugCircle(tile.Center, 5, Color::Red);
		tile.Size += box.Size;
		HitResult result = lineIntersectionBox(start, end, tile);
		if (result.HitLocation != Vector2f(0, 0))
		{
			if (bestResult.HitLocation == Vector2f(0, 0) || VectorDistance(result.HitLocation, start) < VectorDistance(bestResult.HitLocation, start))
			{
				bestResult = result;
			}
		}
	}

	return bestResult;
}

HitResult PhysicsUtilities::boxSweep(const GameWorld* World, Vector2f start, Vector2f end, Vector2f size)
{
	return PhysicsUtilities::boxSweep(World, start, end, AABB(start, size));
}



//check if a box is blocked by a solid surface, the box is defined by the center and the size
bool PhysicsUtilities::isBoxBlocked(const GameWorld* World,Vector2f center, Vector2f size) {
	Vector2f point = center;
	Chunk * chosenchunk = World->getChunk(point);
	if (chosenchunk) {
		float realx = point.x - chosenchunk->getLocation().x;
		float realy = point.y - chosenchunk->getLocation().y;
		float finalx = (realx / TileSize);
		float finaly = (realy / TileSize);
		Vector2f sSize = {(size.x - 0.1f)/2, (size.y - 0.1f)/2};

		return
			isPointBlocked(World, { center.x, center.y }) ||
			isPointBlocked(World, { center.x - sSize.x, center.y + sSize.y }) ||
			isPointBlocked(World, { center.x + sSize.x, center.y + sSize.y }) ||
			isPointBlocked(World, { center.x + sSize.x, center.y - sSize.y }) ||
			isPointBlocked(World, { center.x - sSize.x, center.y - sSize.y })
		;
	}
	return false;
}
bool PhysicsUtilities::isBoxBlocked(const GameWorld* World,AABB box) {
	return isBoxBlocked(World, box.Center, box.Size);
}

//check a circle moving along a line, capsule raycast, returns the hit point
Vector2f PhysicsUtilities::circleSweep(const GameWorld* World,Vector2f start, Vector2f end, float radius) {
	std::vector<std::pair<Vector2f, Vector2f>> rays;
	Vector2f point;

	auto mainA = std::make_pair(Vector2f(start.x, start.y + radius * ((end.y > start.y) * 2 - 1)), Vector2f(end.x, end.y + radius * ((end.y > start.y) * 2 - 1)));
	auto mainB = std::make_pair(Vector2f(start.x, start.y + radius * ((end.x > start.x) * 2 - 1)), Vector2f(end.x, end.y + radius * ((end.x > start.x) * 2 - 1)));
	auto mainMiddle = std::make_pair(Vector2f(start.x, start.y), Vector2f(end.x, end.y)); //I'll improve this XD

	rays.push_back(mainA);
	rays.push_back(mainB);
	rays.push_back(mainMiddle);
	//Add more rays if (radius > TileSize). Rays like the main ones, in between them
	for(auto it = rays.begin(); it != rays.end(); ++it) {
		point = simpleRaycast(World, (*it).first, (*it).second).HitLocation;
		if (point.x != 0 && point.y != 0) return point;
	}
	return Vector2f(0, 0);
}
Vector2f PhysicsUtilities::circleSweep(const GameWorld* World, SimpleCircle circle, Vector2f end) {
	return circleSweep(World, circle.Center, end, circle.Radius);
}


//check a box to other box, return true if the boxes overlap
bool PhysicsUtilities::boxIntersection(Vector2f centerA, Vector2f sizeA, Vector2f centerB, Vector2f sizeB) {
	int n = boxesRelativePosition(centerA, sizeA, centerB, sizeB);
	return ((n < 4)&&(n > 0));
}
bool PhysicsUtilities::boxIntersection(AABB boxA, AABB boxB) {
	return boxIntersection(boxA.Center, boxA.Size, boxB.Center, boxB.Size);
}


//check a circle to other circle, return true if they overlap
bool PhysicsUtilities::circleIntersection(Vector2f centerA, Vector2f centerB, float radiusA, float radiusB) {
	bool intersec;
	float distanceCenters = distancepoints(centerA, centerB);
	float bothRadius = radiusA + radiusB;
	if (distanceCenters >= bothRadius) {
		intersec = false;
	} else {
		intersec = true;
	}
	return intersec;
}

bool PhysicsUtilities::circleIntersection(SimpleCircle circleA, SimpleCircle circleB) {
	return circleIntersection(circleA.Center, circleB.Center, circleA.Radius, circleB.Radius);
}


//Helpers
bool PhysicsUtilities::pointInsideBox(Vector2f point, AABB box) {
	return pointInsideBox(point, box.Center, box.Size);
}

bool PhysicsUtilities::pointInsideBox(Vector2f point, Vector2f center, Vector2f size){
	bool isInside = false;
	if ((point.x < center.x+size.x) && (point.x > center.x-size.x)) {
		if ((point.y < center.y+size.y) && (point.y > center.y-size.y)) {
			isInside = true;
		}
	}
	return isInside;
}

int PhysicsUtilities::boxesRelativePosition(Vector2f centerA, Vector2f sizeA, Vector2f centerB, Vector2f sizeB) {
	//Returns number of vertixes inside the box
	int n = 0; //Number of vertixes of box A inside box B
	Vector2f point;

	//Top left
	point.x = centerA.x + sizeA.x;
	point.y = centerA.y - sizeA.y;
	if (pointInsideBox(point, centerB, sizeB)) n++;

	//Top right
	point.x = centerA.x + sizeA.x;
	point.y = centerA.y + sizeA.y;
	if (pointInsideBox(point, centerB, sizeB)) n++;

	//Bottom left
	point.x = centerA.x - sizeA.x;
	point.y = centerA.y - sizeA.y;
	if (pointInsideBox(point, centerB, sizeB)) n++;

	//Bottom right
	point.x = centerA.x - sizeA.x;
	point.y = centerA.y + sizeA.y;
	if (pointInsideBox(point, centerB, sizeB)) n++;

	return n;
}

int PhysicsUtilities::boxesRelativePosition(AABB boxA, AABB boxB){
	return boxesRelativePosition(boxA.Center, boxA.Size, boxB.Center, boxB.Size);
}

int PhysicsUtilities::lineIntersectionCircle(Vector2f circleCenter, float circleRadius, Vector2f start, Vector2f end, Vector2f& intersectionA, Vector2f& intersectionB) {
	// Transform to local coordinates
	Vector2f LocalP1 = start - circleCenter;
	Vector2f LocalP2 = end - circleCenter;
	// Precalculate this value. We use it often
	Vector2f P2MinusP1 = LocalP2 - LocalP1;

	float a = (P2MinusP1.x) * (P2MinusP1.x) + (P2MinusP1.y) * (P2MinusP1.y);
	float b = 2 * ((P2MinusP1.x * LocalP1.x) + (P2MinusP1.y * LocalP1.y));
	float c = (LocalP1.x * LocalP1.x) + (LocalP1.y * LocalP1.y) - (circleRadius * circleRadius);
	float delta = b * b - (4 * a * c);
	if (delta < 0) { // no intersection
		return 0;
	} else if (delta == 0) { //1 intersection
		float u = -b / (2 * a);
		intersectionA = start + (u * P2MinusP1);
		return 1;
	} else if (delta > 0) { // Two intersections
		float SquareRootDelta = sqrt(delta);
		float u1 = (-b + SquareRootDelta) / (2 * a);
		float u2 = (-b - SquareRootDelta) / (2 * a);
		intersectionA = start + (u1 * P2MinusP1);
		intersectionB = start + (u2 * P2MinusP1);
		return 2;
	}
	return -1; //Just in case....
}

float PhysicsUtilities::distancepoints(Vector2f pointA, Vector2f pointB) {
	return sqrt(pow(pointA.x - pointB.x, 2) + pow(pointA.y - pointB.y, 2));
}

AABB PhysicsUtilities::getTileBounds(const GameWorld* World,Vector2f location)
{
	AABB result;
	Chunk * chosenchunk = World->getChunk(location);

	if (chosenchunk)
	{
		result = chosenchunk->getTileBounds(location);
	}
	return result;
}

HitResult PhysicsUtilities::lineIntersectionBox(Vector2f start, Vector2f end, AABB box)
{
	HitResult result;

	

	//hardcore as fuck maths
	
	//save a few function calls
	const float Top = box.Top();
	const float Down = box.Down();
	const float Left = box.Left();
	const float Right = box.Right();

	//check with top part of box
	if (start.y <=Top && end.y >= Top || start.y <= Top && end.y >= Top) 
	{		
		
		//now calculate the X
		Vector2f diff = end - start;

		float dist = (Top - start.y) / diff.y;
		float x = start.x + dist*diff.x;

		if (x < Right&& x > Left) // check if x is in the segment
		{
			result.HitLocation.x = x;
			result.HitLocation.y = Top;
			result.HitNormal = Vector2f(0, -1);
			return result;
		}
		
	}

	//check with bottom of the box 
	if (end.y <= Down && start.y >=Down || end.y <Down && start.y >= Down)
	{
	
		

		//now calculate the X
		Vector2f diff = end - start;

		float dist = (Down - start.y) / diff.y;
		float x = start.x + dist*diff.x;

		if (x < Right && x > Left) // check if x is in the segment
		{
			result.HitLocation.x = x;
			result.HitLocation.y = Down;
			result.HitNormal = Vector2f(0, +1);
			return result;
		}
		
	}
	//check the right of the box
	if (end.x <= Right && start.x >=Right|| end.x <= Right && start.x >=Right)
	{
		

		//now calculate the Y
		Vector2f diff = end - start;

		float dist = (Right - start.x) / diff.x;
		float y = start.y + dist*diff.y;

		if (y < Down && y > Top) // check if x is in the segment
		{
			result.HitLocation.y = y;
			result.HitLocation.x = Right;
			result.HitNormal = Vector2f(1, 0);
			return result;
		}
	
	}


	//check the left of the box
	if (start.x <= Left && end.x >= Left || start.x <= Left && end.x >= Left)
	{
		
		//now calculate the Y
		Vector2f diff = end - start;

		float dist = (Left - start.x) / diff.x;
		float y = start.y + dist*diff.y;

		if (y < Down && y > Top) // check if x is in the segment
		{
			result.HitLocation.y = y;
			result.HitLocation.x = Left;
			result.HitNormal = Vector2f(-1, 0);
			return result;
		}
		
	}




	
	//return default trash result at 0
	return result;
}

float PhysicsUtilities::VectorDistance(Vector2f Vec1, Vector2f Vec2)
{
	float x = Vec1.x - Vec2.x;
	float y = Vec1.y - Vec2.y;
	return sqrt(x*x + y*y);
}





//Nothing interesting here:

/*Vector2f vertStart(start.x, start.y + radius * ((end.y > start.y) * 2 - 1));
	Vector2f horiStart(start.x, start.y + radius * ((end.x > start.x) * 2 - 1));
	Vector2f vertEnd(end.x, end.y + radius * ((end.y > start.y) * 2 - 1));
	Vector2f horiEnd(end.x, end.y + radius * ((end.x > start.x) * 2 - 1));

	rays.push_back(Vector2f(start.x, start.y + radius * ((end.y > start.y) * 2 - 1))); 
	rays.push_back(Vector2f(start.x, start.y + radius * ((end.x > start.x) * 2 - 1)));
	rays.push_back(Vector2f(end.x, end.y + radius * ((end.y > start.y) * 2 - 1))); 
	rays.push_back(Vector2f(end.x, end.y + radius * ((end.x > start.x) * 2 - 1))); */

	